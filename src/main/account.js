import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Dimensions, ScrollView,
  Image
} from 'react-native';

var {height, width} = Dimensions.get('window');
export default class AccScreen extends Component {
  _keyExtractor = (item, index) => item;
  constructor(props)
  {

    super(props);

    this.state = {

      dataSource : ['Notification','Downloads Preferences','Contact Us', 'Settings']

    }
  }




  render() {
    return (
   
        <View style={styles.container}>  
              <View style = {{marginTop : 50, height : 200, backgroundColor : 'white' ,}}> 
                    <Text style = {styles.usernameText}>Hi,there</Text>
                    <Text style = {styles.signInInfoText} numberOfLines = {2}>Want to save your favorite books and newspapers ?</Text>
                    
                    <TouchableHighlight style = {styles.signInButton}>

                       <Text style = {styles.signInText}>Sign In</Text>

                    </TouchableHighlight>

              </View>
              <View style = {{marginTop : 1}}> 

                       <FlatList style = {{backgroundColor : 'white'}} 
                        scrollEnabled = {false}
                        indicatorStyle = "white"
                        data={this.state.dataSource}
                        extraData={this.state}
                        keyExtractor={this._keyExtractor}
                        renderItem={({item}) => (
                        
                        <TouchableHighlight style = {styles.listView}>
                
                            
                            <Text style = {styles.listText}>{item}</Text>
                            {/* <Image source={require('./my-icon.png')} /> */}
                            
                        </TouchableHighlight>
                        )}
                        /> 

              </View>
    
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop : 20,
    backgroundColor: 'white',
  },
  mainView : {

  },
  usernameText : {
    fontSize : 25,
    fontWeight : '800',
    textAlign : 'left',
    marginLeft : 20,

  },
  listView : {
    flexDirection : 'row',
    backgroundColor: 'white',
    marginLeft : 10,
    marginRight : 1, 
    height : 70, 
    marginTop : 1,
    borderBottomWidth : 0.8,
    borderBottomColor : 'lightgrey'
  },
  listText : {
    fontSize : 16,
    fontWeight : '700',
    textAlign : 'left',
    marginLeft : 10,
    marginTop : 25

  },
  signInInfoText: {
    marginTop : 15,
    marginLeft : 20,
    marginRight : 100,
    fontSize : 17,
    fontWeight : '400',
    textAlign : 'left',
  

  },

  signInButton: {

    marginTop : 30,
    marginLeft : 15,
    marginRight : 15,

    height : 55,
    borderRadius : 50,
    backgroundColor : '#f9c806'

  },

  signInText : {

    marginTop : 15,
    marginLeft : 20,
    marginRight : 20,
    fontSize : 18,
    fontWeight : '700',
    textAlign : 'center',
    color : 'white'

  }

});

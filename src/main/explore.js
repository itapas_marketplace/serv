import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Dimensions, ScrollView
} from 'react-native';

var {height, width} = Dimensions.get('window');
export default class ExploreScreen extends Component {
  _keyExtractor = (item, index) => item;
  constructor(props)
  {

    super(props);

    this.state = {

      dataSource : ['i ','am','awesome', 'mem', 'memeew', 'lelee']

    }
  }




  render() {
    return (
   
        <ScrollView style={styles.container}>  

              <View style = {{marginTop : 20}}> 
                    <Text style = {styles.headerText}>Top 10 today on Buku</Text>
                    <View style = {{marginTop : 10}}>

                              <FlatList style = {{backgroundColor : 'white', flex : 1}} 
                                  horizontal = {true}
                                  indicatorStyle = "white"
                                  data={this.state.dataSource}
                                  extraData={this.state}
                                  keyExtractor={this._keyExtractor}
                                  renderItem={({item}) => (
                                  
                                    <TouchableHighlight style = {styles.assetView}>
                                      
                                        <Text>{item}</Text>
                                      
                                    </TouchableHighlight>
                                  )}
                                  />


                    </View>


              </View>
              <View style = {{marginTop : 30}}> 
                    <View style = {{flexDirection : 'row'}}>
                          <Text style = {styles.headerText}>Top Interests</Text>
                          <Text style = {{marginLeft : 190, marginTop : 10}}>View all</Text>
                    </View>
                    <View style = {{marginTop : 10}}>

                              <FlatList style = {{backgroundColor : 'white', flex : 1, marginLeft : 5}} 
                                  horizontal = {true}
                                  indicatorStyle = "white"
                                  data={this.state.dataSource}
                                  extraData={this.state}
                                  keyExtractor={this._keyExtractor}
                                  renderItem={({item}) => (
                                  
                                    <TouchableHighlight style = {styles.assetView2}>
                                      
                                        <Text>{item}</Text>
                                      
                                    </TouchableHighlight>
                                  )}
                                  />


                    </View>


              </View>
              <View style = {{marginTop : 30}}> 
                    <Text style = {styles.headerText}>Islamic </Text>
                    <View style = {{marginTop : 10}}>

                              <FlatList style = {{backgroundColor : 'white', flex : 1}} 
                                  horizontal = {true}
                                  indicatorStyle = "white"
                                  data={this.state.dataSource}
                                  extraData={this.state}
                                  keyExtractor={this._keyExtractor}
                                  renderItem={({item}) => (
                                  
                                    <TouchableHighlight style = {styles.assetView2}>
                                      
                                        <Text>{item}</Text>
                                      
                                    </TouchableHighlight>
                                  )}
                                  />


                    </View>


              </View>
              <View style = {{marginTop : 30}}> 
                    <Text style = {styles.headerText}>Recommaded for you</Text>
                    <View style = {{marginTop : 10}}>

                              <FlatList style = {{backgroundColor : 'white', flex : 1}} 
                                  horizontal = {true}
                                  indicatorStyle = "white"
                                  data={this.state.dataSource}
                                  extraData={this.state}
                                  keyExtractor={this._keyExtractor}
                                  renderItem={({item}) => (
                                  
                                    <TouchableHighlight style = {styles.assetView2}>
                                      
                                        <Text>{item}</Text>
                                      
                                    </TouchableHighlight>
                                  )}
                                  />


                    </View>


              </View>

          

        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop : 20,
    backgroundColor: 'white',
  },
  mainView : {

  },
  headerText : {
    fontSize : 20,
    fontWeight : '800',
    textAlign : 'left',
    marginLeft : 10
  },
  assetView : {
    backgroundColor: 'yellow',
    width : 180, 
    height : 280, 
    margin : 5,
    borderRadius : 4
  },
  assetView2 : {
    backgroundColor: 'yellow',
    width : 150, 
    height : 250, 
    margin : 5,
    borderRadius : 4
  }
});
